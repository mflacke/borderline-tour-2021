# Tracking the todo list

## Routing - wo fahre ich her, wie navigiere ich, womit navigiere ich, ueben, testen ...
* [ ] mit fast keinem Strom auskommen (laden im Cafe): powerbank, die stark und schnell ladbar ist
* [x] Navi app kompatibel mit Brouter: locus maps kaufen (pro version, 9.99 Euro) (2020-01-17)
* [ ] Locus maps testen und ueben
* [ ] Karten laden fuer locus maps
* [ ] Gesamt-GPX in Teile teilen, denn sonst explodiert Locus maps. Siehe [GPX directory](gpx)
* [ ] 

## Wie uebernachten? Ich brauche ein leichtes Zelt
* [ ] schoen leicht, stabil, und gut getarnt, ich will ja wild campen
* [ ] ich werde trotzdem den dicken Schlafsack und die Isomatte mitnehmen muessen
* [ ] 


## Fahrrad: kriege ich mein altes Rennrad wieder zum Rennen?
* [ ] Wer repariert mir so etwas?
* [ ] Ich sollte vielleicht vorher mal wieder ein paar Touren absolvieren, um auf Touren zu kommen
* [ ] 


## Wann fahre ich los? es soll ja nicht soo kalt sein nachts
* auf jeden Fall mit Sommerzeit (also Ende Maerz)
* Corona darf auch nicht hindern, die Pensionen sollten wieder auf haben (evtl beruflich unterwegs? :))
* Nachttemperaturen sollten nicht unter 10 Grad sein
* Es sollte nicht zu viel regnen
* -> erst im Mai?
 
