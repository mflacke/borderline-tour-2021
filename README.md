# Grenzwertig 2021 (former known as borderline tour)

... klingt haarstraeubend, aber tatsaechlich ist nicht mehr gemeint, als die [4500 km deutsche Grenze](https://www.destatis.de/DE/Themen/Laender-Regionen/Tabellen/gemeinsame-grenzen-deutschlands.html
) mit dem Fahrrad abzufahren ...


### Route

Gefahren wird im Uhrzeigersinn, also erst die Westgrenze, dann an der Nordsee und Ostsee entlang. 
Die Oder-Neisse-Grenze runter bis Zittau, dann ueber das Erzgebirge zur europaeischen Wasserscheide beim Fichtelgebirge.
Ueber die Oberpfalz und den bayrischen Wald dann nach Passau. An der oesterreichischen Grenze nach Salzburg, dann im Alpenvorland nach Lindau.
Am Bodensee entlang nach Loerrach, dann entweder den Rhein entlang nach Karlsruhe oder ueber den Schwarzwald.

### Teilbereiche

1. K126 - Emden		1057 km
2. Emden - Niebuell	807 km
3. Niebuell - Ahlbeck	777 km
4. Ahlbeck - Zittau	
5. Zittau - Fichtelgeb.	
5. Fichtelgeb - Passau	
6. Passau - Salzburg	
7. Salzburg - Lindau	380 km
8. Lindau - Loerrach	
9. Loerrach - Karlsruhe	
10. Karlsruhe - K126

Summe			
